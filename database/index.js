const fs = require('fs');
const path = require('path');
const Sequelize = require('sequelize');
const tracer = require('tracer');

const config = require('../config');

let db = {};

const sequelize = new Sequelize(config.db.database, config.db.username, config.db.password, {
  host: config.db.host,
  dialect: config.db.dialect,
  logging: global.logger && global.logger.debug
});

fs.readdirSync(config.models.path)
  .filter(function(file) {
    return (file.indexOf('.') !== 0) && (file.slice(-3) === '.js');
  })
  .forEach(function(file) {
    let model = sequelize['import'](path.join(config.models.path, file));

    db[model.name] = model;
  });

Object.keys(db).forEach(function(modelName) {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

/*db.Sticker.hasOne(db.User, {
  foreignKey: 'fk_stickers_user_id',
  as: 'user_id'
});*/

/*db.User.hasMany(db.Sticker, {
  foreignKey: 'user_id',
  as: 'id'
});*/

db.sequelize = sequelize;
db.Sequelize = Sequelize;

module.exports = db;