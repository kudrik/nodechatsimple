const router = require('koa-router')();
const bodyParser = require('koa-bodyparser');
const cors = require('koa-cors');
const views = require('koa-views');
const session = require('koa-generic-session');
const serve = require('koa-static');


const Application = require('./bootstrap/Application');
const {User} = require('./database');


const app = new Application();

app.use(cors());
app.use(require('./api/middlewares/responseLog'));
app.use(require('./api/exceptions/Handler'));
app.use(bodyParser());
app.use(require('./api/middlewares/responses'));
app.use(serve(__dirname + '/public'));

app.keys = ['secret'];
app.use(session({}));
app.use(require('./api/middlewares/loadUser'));

app.use(views('./views', {
  default: 'jade'
}));


const routes = require('./api/routes');

for (let route in routes) {
  const [method, path] = route.split(' ');
  const funcName = method.toLowerCase();

  const [controllerName, action] = routes[route].split('@');
  const controller = new (require('./api/controllers/' + controllerName))();

  if (router[funcName] && controller[action]) {
    router[funcName](path, controller[action]);
  }
}

app.use(router.routes());
app.use(router.allowedMethods());

/*
const bcrypt = require('bcrypt');

var x = bcrypt.genSaltSync(10);
console.log(bcrypt.hashSync('123456', x), x);
*/

module.exports = app;