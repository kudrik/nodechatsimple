const config = require('../config');
const _ = require('lodash');
const md5 = require('md5');
const {User} = require('../database');

module.exports = function(http) {

    const io = require('socket.io')(http);

    var users = {};
    var usersAndSockets = {
        users : {},
        sockets:{}
    };

    io.on('connection', function (socket) {

        //when somebodyu connected retrun list curent users
        socket.emit('users',users);

        //when connected new auth user
        socket.on('user online', function(username) {

            //add to user list and send notfi to curent and broadcast
            if (username) {
                User.findOne({where:{ username: username }}).then(function(user) {

                    usersAndSockets.sockets[socket.id] = user.username;
                    if (!usersAndSockets.users[user.username]) {
                        usersAndSockets.users[user.username] = {}
                    }
                    usersAndSockets.users[user.username][socket.id]=true;

                    users[user.username] = user.username;

                    socket.emit('users',users);
                    socket.broadcast.emit('users',users);
                });
            }
        });

        socket.on('disconnect', function(){

            var username = usersAndSockets.sockets[socket.id];
            delete usersAndSockets.sockets[socket.id];
            if (usersAndSockets.users[username] && usersAndSockets.users[username][socket.id]) {
                delete usersAndSockets.users[username][socket.id];
            }

            if (_.isEmpty(usersAndSockets.users[username])) {
                delete users[username];
            }

            socket.broadcast.emit('users',users);
        });

        //listning signal from http server
        socket.on('new message', function(data){

            var comptoken = config.socket.secret;
            for (var row in data.data) {
                comptoken = comptoken + '_' + row;
            }

            if (data.data && data.token && data.token==md5(comptoken)) {
                //transer to all client
                socket.broadcast.emit('new message',data.data);
            }
        });
    });
};