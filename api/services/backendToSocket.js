const config = require('../../config');
const md5 = require('md5');

module.exports = function(method, data) {

    var token = config.socket.secret;

    //add hash to data
    for (var row in data) {
        token = token + '_' + row;
    }

    //connect to our socket like client and send notif that we have new msg
    var socetClient = require('socket.io-client')('http://'+config.host+':'+config.port);
    socetClient.emit(method, {
        data: data,
        token: md5(token),
    });
};