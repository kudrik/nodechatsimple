const {Chat, User} = require('../../database');

class HomeController {
	*index() {
		yield this.render('index.jade', {
			user: this.user,
			items: yield Chat.findAll({
				include: [ User ],
				order: [
					['id','DESC']
				],
				limit:10,
			}),
		});
	}
}

module.exports = HomeController;