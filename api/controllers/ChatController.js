const _ = require('lodash');
const {Chat, User} = require('../../database/index');
const BadRequestHttpException = require('../exceptions/BadRequestHttpException');
const UnauthorizedHttpException = require('../exceptions/UnauthorizedHttpException');

class ChatController {

  *create() {

    const {comment} = this.request.body;

    if (!this.user) {
      throw new UnauthorizedHttpException();
    }

    var chat = yield this.user.createChat({comment:comment});

    if (!chat) {
      throw new BadRequestHttpException();
    }

    //connect to our socket like client and send notif that we have new msg
    require('../services/backendToSocket')('new message',{
      username:this.user.username,
      comment:chat.comment,
    });

    this.ok();
  }


  *list() {

    const {page} = this.request.query;

    var results = yield Chat.pagination({
        page: page,
        onpage: 10,
      },
      {
        include: [User],
        order: [
          ['id', 'ASC']
        ],
      });

    yield this.render('history.jade', {
      items: results.items,
      pages: results.pages,
      page: results.page,
    });
  }
}

module.exports = ChatController;