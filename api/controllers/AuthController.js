const bcrypt = require('bcrypt');
const {User} = require('../../database');
const UnauthorizedHttpException = require('../exceptions/UnauthorizedHttpException');

class AuthController {

    *loginOrCreate() {
        const {username, password} = this.request.body;

        var userArr = yield User.findOrCreate({
            where:{ username: username },
            defaults: { password:password }
        });

        var user = userArr[0];

        if (userArr[1] || user.checkPassword(password)) {

            this.session.user = user.id;

        } else {
            throw new UnauthorizedHttpException();
        }

        this.redirect('/');
    }

    *logout() {
        delete this.session.user;
        this.redirect('/');
    }
}

module.exports = AuthController;