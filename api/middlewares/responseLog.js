const responseLog = function *(next) {
  this.res.once('finish', () => {
    logger.info(`${this.request.method} ${this.request.url} ${this.status}`);
  });

  return yield next;
};

module.exports = responseLog;