const responses = function *(next) {
  this.created = (data = {}, errors = []) => {
    this.body = {
      success: true,
      data,
      errors
    };

    this.status = 200;
  };

  this.ok = (data = {}, errors = []) => {
    this.body = {
      success: true,
      data,
      errors
    };

    this.status = 200;
  };

  return yield next;
};

module.exports = responses;