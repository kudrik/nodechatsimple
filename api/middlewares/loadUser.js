const {User} = require('../../database');

module.exports = function *(next) {

    this.user = null;

    if (this.session.user) {
        this.user = yield User.findById(this.session.user);
    }

    return yield next;
}
