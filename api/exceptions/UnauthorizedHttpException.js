class UnauthorizedHttpException extends Error {
  constructor(props) {
    super(props);

    this.type = 'unauthorized_error';
    this.status = 401;
  };

  toJSON() {
    return {
      type: this.type,
      message: this.message
    }
  }
}

module.exports = UnauthorizedHttpException;