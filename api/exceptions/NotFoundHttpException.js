class NotFoundHttpException extends Error {
  constructor(props) {
    super(props);

    this.type = 'not_found_error';
    this.status = 404;
  };

  toJSON() {
    return {
      type: this.type,
      message: this.message
    }
  }
}

module.exports = NotFoundHttpException;