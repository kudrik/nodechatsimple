const _ = require('lodash');
const {ValidationError} = require('sequelize/lib/errors');
const BadRequestHttpException = require('./BadRequestHttpException');
const ForbiddenHttpException = require('./ForbiddenHttpException');
const NotFoundHttpException = require('./NotFoundHttpException');
const UnauthorizedHttpException = require('./UnauthorizedHttpException');

const dontReport = [
  ValidationError,
  BadRequestHttpException,
  ForbiddenHttpException,
  NotFoundHttpException,
  UnauthorizedHttpException
];

const Handler = function *(next) {
  try {
    yield next;
  } catch (err) {
    this.status = err.status || 500;
    this.body = {
      success: false,
      data: {},
      errors: err.errors || [err.message || err]
    };
    
    for (let silent of dontReport) {
      if (err instanceof silent) {
        return;
      }
    }

    logger.error(err);
  }
};

module.exports = Handler;