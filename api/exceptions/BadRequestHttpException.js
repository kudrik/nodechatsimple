class BadRequestHttpException extends Error {
  constructor(props) {
    super(props);

    this.type = 'bad_request_error';
    this.status = 400;
  };

  toJSON() {
    return {
      type: this.type,
      message: this.message
    }
  }
}

module.exports = BadRequestHttpException;