class ForbiddenHttpException extends Error {
  constructor(props) {
    super(props);

    this.type = 'forbidden_error';
    this.status = 403;
  };

  toJSON() {
    return {
      type: this.type,
      message: this.message
    }
  }
}

module.exports = ForbiddenHttpException;