const bcrypt = require('bcrypt');

module.exports = function (sequelize, DataTypes) {
  const User = sequelize.define('User', {
    username: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'validation.notEmpty'
        }
      }
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'validation.notEmpty'
        }
      }
    },
  }, {
    tableName: 'users',
    updatedAt: 'updatedAt',
    createdAt: 'createdAt',
    timestamps: false,
    underscored: true,
    hooks: {
      beforeCreate(user) {
        user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
      },
      beforeSave(user) {
        const changed = user.changed();

        if (changed.indexOf('password') !== -1) {
          user.password = bcrypt.hashSync(user.password, bcrypt.genSaltSync(10));
        }
      }
    },
    classMethods: {
      associate: function(models) {
        User.hasMany(models.Chat, {as: 'chats'});
      }
    },
    instanceMethods: {
      toJSON: function () {
        var values = this.get();

        delete password;

        return values;
      },
      checkPassword: function(value) {
        if (bcrypt.compareSync(value, this.password))
          return this;
        else
          return false;
      }
    }
  });

  return User;
};