module.exports = function (sequelize, DataTypes) {
  const Chat = sequelize.define('Chat', {
    comment: {
      type: DataTypes.TEXT,
      allowNull: false,
      validate: {
        notEmpty: {
          msg: 'validation.notEmpty'
        }
      }
    },
    user_id: {
      type: DataTypes.INTEGER
    }
  }, {
    tableName: 'chat',
    updatedAt: 'updatedAt',
    createdAt: 'createdAt',
    timestamps: true,
    underscored: true,
    classMethods: {
      associate: function(models) {
        Chat.belongsTo(models.User);
      },
      pagination: function(pageParam, param = {}) {

        return Chat.count(param).then(function(c) {

          pageParam.page = parseInt(pageParam.page);

          if (!pageParam.page || pageParam.page<1) {
            pageParam.page = 1;
          }

          var pages = c/pageParam.onpage;
          if (parseInt(pages)!=pages) {
            pages = parseInt(pages)+1;
          }

          if (pageParam.page>pages && pages>0) {
            pageParam.page = pages;
          }

          param.offset = (pageParam.page-1)*pageParam.onpage;
          param.limit = pageParam.onpage;

          return Chat.findAll(param).then(function(results) {

            return { items: results, count: c, pages: pages, page: pageParam.page }
          });
        });
      }
    }
  });

  return Chat;
};