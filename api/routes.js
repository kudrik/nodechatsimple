module.exports = {
  'GET /': 'HomeController@index',
  'POST /loginOrCreate': 'AuthController@loginOrCreate',
  'POST /logout': 'AuthController@logout',
  'POST /chat/create': 'ChatController@create',
  'GET /history': 'ChatController@list',

};