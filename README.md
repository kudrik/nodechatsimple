# Chat Nodejs #

* Version 1.0.0


### Installation ###

* clone the repository into folder
* run npm install
```
#!bash

npm install
```

* rename config file from config/index.js.example to config/index.js and setup config variables. (DB connect, http host and port)

* run DB migrations

```
#!bash

npm run migrations
```

### Run project ###
```
#!bash

npm start
```

### Requirements ###
* nodeJs v7.0.0
* npm 3.10.8
* Database Mysql