$(function() {

  // Initialize varibles
  var $window = $(window);
  var usernameId = '#username'; // Input for username
  var messagesId = '#messages'; // Messages area
  var usersBlockId = '#userslist';
  var inputMessageId = '#inputMessage'; // Input message input box


  var socket = io();

  // Sends a chat message
  function sendMessage () {

    // Prevent markup from being injected into the message
    var comment = cleanInput($(inputMessageId).val());

    if (comment) {
      $.post('/chat/create', {comment:comment}).fail(function() {
        alert('error sending message');
      });
      $(inputMessageId).val('');
    }
  }

  function addMessageElement (el, options) {
    var $el = $(el);

    var messages = $(messagesId);

    var rows = messages.find('p');


    if (rows.length>=10) {
      rows.last().fadeOut(500, function() {
          this.remove();
        });
    }

    messages.prepend($el);
  }

  // Prevents input from having injected markup
  function cleanInput (input) {
    return $('<div/>').text(input).text();
  }

  // Adds the visual chat message to the message list
  function addChatMessage (data, options) {

    var username = $(usernameId).text();

    var classname = 'e-name';
    if (String(data.username)==String(username)) {
      classname = classname + ' e-selected';
    }

    var $usernameDiv = $('<span class="'+classname+'"/>').text(data.username+':');

    var $messageBodyDiv = $('<span>').text(' '+data.comment);

    var $messageDiv = $('<p/>').append($usernameDiv, $messageBodyDiv);

    addMessageElement($messageDiv, options);
  }

  function updateUsersBlock(users) {
    var usersBlock = $(usersBlockId);
    usersBlock.html('');

    for (var user in users) {
      usersBlock.append($('<p>').text(user));
    }
  }

  // Keyboard events
  $window.keydown(function (event) {

    // When the client hits ENTER on their keyboard
    if (event.which === 13) {
      sendMessage();
    }
  });

  //
  socket.on('connect', function(){

    var username = $('#username').text();

    if (username) {
      socket.emit('user online',username);
    }
    //socket.emit('get users', null);
  });

  // Whenever the server emits 'new message', update the chat body
  socket.on('new message', function (data) {
    addChatMessage(data);
  });

  // Whenever the server emits 'typing', show the typing message
  socket.on('myerror', function (data) {
    alert (data);
  });

  socket.on('users', function (users) {
    updateUsersBlock(users);
  });


});