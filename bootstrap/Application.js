const Koa = require('koa');
const config = require('../config');
const db = require('../database');

class Application extends Koa {
  *start() {
    yield new Promise((resolve, reject) => {
      this.server = this.listen(config.port, config.host, err => err ? reject(err) : resolve());
      logger.log('Listening on host %s:%s', config.host, config.port);

      //Socket
      require('../socket')(this.server);
    });
  }

  stop() {
    this.server.close();
  }

  created(body) {
    this.status = 201;
    this.errors = [];
    this.body = body || {}
  }
}

module.exports = Application;